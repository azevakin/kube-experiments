from django.contrib import admin

from cities import models


@admin.register(models.City)
class CityAdmin(admin.ModelAdmin):
    list_display = ["name"]
    search_fields = list_display
