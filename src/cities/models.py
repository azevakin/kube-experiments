from django.db import models


class City(models.Model):
    city_id = models.BigIntegerField(primary_key=True)
    country_id = models.BigIntegerField()
    region_id = models.BigIntegerField()
    name = models.CharField(max_length=128)

    class Meta:
        db_table = "cities"
