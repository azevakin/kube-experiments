from django.views.generic import ListView

from cities.models import City


class Index(ListView):
    model = City
    template_name = "index.html"
    paginate_by = 25

    def get_queryset(self):
        return super().get_queryset().order_by("?")
