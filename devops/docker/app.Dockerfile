###########
# BUILDER #
###########

# pull official base image
FROM python:3.9.7-alpine3.14 as build

RUN apk --update add gcc musl-dev libffi-dev curl\
 make # need for build uvloop

ENV PYTHONUNBUFFERED=1\
 PYTHONDONTWRITEBYTECODE=1\
 # https://python-poetry.org/docs/configuration/#using-environment-variables
 POETRY_VERSION=1.1.10\
 POETRY_HOME="/opt/poetry"\
 POETRY_VIRTUALENVS_CREATE=false\
 POETRY_NO_INTERACTION=true

RUN curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/install-poetry.py | python

WORKDIR /tmp

ADD pyproject.toml poetry.lock ./

RUN /opt/poetry/bin/poetry install

#########
# FINAL #
#########

# pull official base image
FROM python:3.9.7-alpine3.14

# create the app user
#RUN addgroup -S app && adduser -S app -G app

COPY --from=build /usr/local /usr/local

# copy project
COPY src /opt/app

# chown all the files to the app user
#RUN chown -R app:app /opt/app

# change to the app user
#USER app

WORKDIR /opt/app

CMD  ["sh", "-c", "python manage.py check"]
